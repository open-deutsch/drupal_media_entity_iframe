<?php

namespace Drupal\ld_media_entity_iframe\Plugin\media\Source;

use Drupal\media\MediaSourceBase;

/**
 * IFrame entity media source.
 *
 * @MediaSource(
 *   id = "iframe",
 *   label = @Translation("IFrame"),
 *   description = @Translation("Shows arbitrary URL content in an iframe."),
 *   allowed_field_types = {"link"},
 *   default_thumbnail_filename = "iframe.png"
 * )
 */
class IFrame extends MediaSourceBase {

  /**
   * Key for "Title" metadata attribute.
   *
   * @var string
   */
  const METADATA_ATTRIBUTE_Title = 'title';

  /**
   * {@inheritdoc}
   */
  public function getMetadataAttributes () {
    return [
        static::METADATA_ATTRIBUTE_Title => $this->t('Title'),
    ];
  }

}
