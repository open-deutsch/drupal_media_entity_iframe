<?php

namespace Drupal\ld_media_entity_iframe\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the iframe formatter.
 *
 * @FieldFormatter(
 *   id = "iframe",
 *   label = @Translation("IFrame content"),
 *   field_types = {
 *     "link",
 *     "string",
 *     "string_long",
 *   },
 * )
 */
class IFrameFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function viewElements (FieldItemListInterface $items, $langcode) {
    $element = [];
    foreach ($items as $delta => $item) {
      $value = $item->getValue(); //array keys: uri,title,options,_attributes
      $url = $value['uri'];
      $element[$delta] = [
        '#markup' => $this->markup($url),
        '#allowed_tags' => [
          'iframe',
          'a',
          'p',
        ],
      ];
    }
    return $element;
  }

  private function  markup ($url) {
    $config = \Drupal::config('ld_media_entity_iframe.settings');
    $width = $config->get('width_percent');
    $height = $config->get('height_pixels');
    return "<iframe src=\"{$url}\" height=\"{$height}\" width=\"{$width}%\"></iframe>
<p><small>
  Der Inhalt, den du in diesem Rahmen siehst, stammt von <a href=\"{$url}\">{$url}</a> und ist hier bei uns per <a href=\"https://de.wikipedia.org/wiki/Hotlinking\">Hotlinking</a> eingebettet.
</small></p>";
  }

  /**
   * {@inheritdoc}
   */
  public static function create (ContainerInterface $container,
      array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('config.factory')
    );
  }

}
