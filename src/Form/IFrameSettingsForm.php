<?php

namespace Drupal\ld_media_entity_iframe\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configuration form definition for iframes.
 *
 * @author h2b
 */
class IFrameSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames () {
    return [
        'ld_media_entity_iframe.settings'
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId () {
    return 'media_entity_iframe_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm (array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('ld_media_entity_iframe.settings');
    $form['width_percent'] = [
        '#type' => 'number',
        '#title' => $this->t('Width:'),
        '#description' => $this->t('The width of the iframe in percent of the available page width.'),
        '#field_suffix' => $this->t('%'),
        '#default_value' => $config->get('width_percent'),
    ];
    $form['height_pixels'] = [
        '#type' => 'number',
        '#title' => $this->t('Height:'),
        '#description' => $this->t('The height of the iframe in pixels.'),
        '#field_suffix' => $this->t('pixels'),
        '#default_value' => $config->get('height_pixels'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm (array &$form, FormStateInterface $form_state){
    $config = $this->config('ld_media_entity_iframe.settings');
    $config->set('width_percent', $form_state->getValue('width_percent'));
    $config->set('height_pixels', $form_state->getValue('height_pixels'));
    $config->save();
    return parent::submitForm($form, $form_state);
  }

}
